//
//  UIFont + Extention.swift
//  MyChat
//
//  Created by Михаил Колотилин on 06.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

extension UIFont {
    static func avenir20() -> UIFont? {
        return UIFont.init(name: "avenir", size: 20)
    }
    static func avenir26() -> UIFont? {
        return UIFont.init(name: "avenir", size: 26)
    }
    static func laoSangamMN18() -> UIFont? {
        return UIFont.init(name: "Lao Sangam MN", size: 18)
    }
    static func laoSangamMN20() -> UIFont? {
        return UIFont.init(name: "Lao Sangam MN", size: 20)
    }
    static func americanTypewriter40() -> UIFont? {
        return UIFont(name: "AmericanTypewriter", size: 40)
    }
    
}
