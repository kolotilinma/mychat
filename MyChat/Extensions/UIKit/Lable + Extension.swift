//
//  Lable + Extension.swift
//  MyChat
//
//  Created by Михаил Колотилин on 06.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

extension UILabel {
    convenience init(text: String,
                     font: UIFont? = .avenir20()) {
        self.init()
        self.font = font
        self.text = text
    }
}
