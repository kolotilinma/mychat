//
//  StackView + Extention.swift
//  MyChat
//
//  Created by Михаил Колотилин on 06.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

extension UIStackView {
    convenience init(arrangedSubviews: [UIView], axis: NSLayoutConstraint.Axis, spacing: CGFloat) {
        self.init(arrangedSubviews: arrangedSubviews)
        self.axis = axis
        self.spacing = spacing
    }
}
