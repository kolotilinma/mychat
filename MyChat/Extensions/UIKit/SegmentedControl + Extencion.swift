//
//  SegmentedControl + Extencion.swift
//  MyChat
//
//  Created by Михаил Колотилин on 08.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

extension UISegmentedControl {
    convenience init(first: String, second: String) {
        self.init()
        
        self.insertSegment(withTitle: first, at: 0, animated: true)
        self.insertSegment(withTitle: second, at: 0, animated: true)
        self.selectedSegmentIndex = 0
    }
}
