//
//  SelfConfiguringCell.swift
//  MyChat
//
//  Created by Михаил Колотилин on 10.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

protocol SelfConfiguringCell {
    static var reuseId: String { get }
    func configure<U: Hashable>(with value: U)
}
