//
//  WaitingChatsNavigation.swift
//  MyChat
//
//  Created by Михаил Колотилин on 15.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import Foundation

protocol WaitingChatsNavigation: class {
    func removeWaitingChat(chat: MChat) 
    func changeToActive(chat: MChat)
}
