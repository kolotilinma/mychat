//
//  AuthNavigationDelegate.swift
//  MyChat
//
//  Created by Михаил Колотилин on 13.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import Foundation

protocol AuthNavigatingDelegate: class {
    func toLoginVC()
    func toSignUpVc()
}
