//
//  MainTabBarController.swift
//  MyChat
//
//  Created by Михаил Колотилин on 08.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    private let currentUser: MUser
    
    init(currentUser: MUser = MUser(firstname: "Test",
                                    lastname: "Test",
                                    email: "test@mail.c",
                                    avatarStringURL: "askdjhaksdjaasdasfsdf",
                                    description: "dsfgdfghsgfhgdsh",
                                    sex: "mmm",
                                    id: "123125465sdfg3123")) {
        self.currentUser = currentUser
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let listViewController = ListViewController(currentUser: currentUser)
        let peopleViewController = PeopleViewController(currentUser: currentUser)
        
        tabBar.tintColor = #colorLiteral(red: 0.5568627451, green: 0.3529411765, blue: 0.968627451, alpha: 1)
        let boldConfig = UIImage.SymbolConfiguration(weight: .medium)
        let convImage = UIImage(systemName: "bubble.left.and.bubble.right", withConfiguration: boldConfig)!
        let peopleImage = UIImage(systemName: "person.2", withConfiguration: boldConfig)!
        
        viewControllers = [
            generateNavigationController(rootViewController: peopleViewController, title: NSLocalizedString("People",comment: "MainTabBarController TabBar"), image: peopleImage),
            generateNavigationController(rootViewController: listViewController, title: NSLocalizedString("Conversations", comment: "MainTabBarController TabBar"), image: convImage)
        ]
    }
    
    private func generateNavigationController(rootViewController: UIViewController, title: String, image: UIImage) -> UIViewController {
        let navigationVC = UINavigationController(rootViewController: rootViewController)
        navigationVC.tabBarItem.title = title
        navigationVC.tabBarItem.image = image
        return navigationVC
    }
}

// MARK: - SwiftUI
import SwiftUI

struct TabBarVCProvider: PreviewProvider {
    static var previews: some View {
        ContainerView().edgesIgnoringSafeArea(.all)
    }
    
    struct ContainerView: UIViewControllerRepresentable {
        
        let tabBarVC = MainTabBarController()
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<TabBarVCProvider.ContainerView>) -> MainTabBarController {
            return tabBarVC
        }
        
        func updateUIViewController(_ uiViewController: TabBarVCProvider.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<TabBarVCProvider.ContainerView>) {
            
        }
    }
}
