//
//  AuthError.swift
//  MyChat
//
//  Created by Михаил Колотилин on 12.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import Foundation

enum AuthError {
    case notFilled
    case invalidEmail
    case passwordNotMatched
    case unknownError
    case serverError
}

extension AuthError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notFilled:
            return NSLocalizedString("Fill in all the fields", comment: "")
        case .invalidEmail:
            return NSLocalizedString("Email format is not valid", comment: "")
        case .passwordNotMatched:
            return NSLocalizedString("Passwords do not match", comment: "")
        case .unknownError:
            return NSLocalizedString("Unknown error", comment: "")
        case .serverError:
            return NSLocalizedString("Server error", comment: "")
        }
    }
}
