//
//  Validators.swift
//  MyChat
//
//  Created by Михаил Колотилин on 12.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import Foundation

class Validators {
    
    static func isFilled(email: String?, password: String?, confirmPassword: String?) -> Bool {
        guard let password = password,
        let confirmPassword = confirmPassword,
        let email = email,
        password != "",
        confirmPassword != "",
            email != "" else {
                return false
        }
        return true
    }
    
    static func isFilled(name: String?, lastname: String?, description: String?, sex: String?) -> Bool {
        guard let description = description,
        let sex = sex,
        let name = name,
        let lastname = lastname,
        description != "",
        sex != "",
            name != "", lastname != "" else {
                return false
        }
        return true
    }
    
    static func isSimpleEmail(_ email: String) -> Bool {
        let emailRegEx = "^.+@.+\\..{2,}$"
        return check(text: email, regEx: emailRegEx)
    }
    
    private static func check(text: String, regEx: String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
        return predicate.evaluate(with: text)
    }
    
}
