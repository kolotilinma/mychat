//
//  UserError.swift
//  MyChat
//
//  Created by Михаил Колотилин on 13.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import Foundation

enum UserError {
    case notFilled
    case photoNotExist
    case cannotGetUserInfo
    case cannotUnwrapToMUser
}

extension UserError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notFilled:
            return NSLocalizedString("Fill in all the fields", comment: "")
        case .photoNotExist:
            return NSLocalizedString("User did not select photo", comment: "")
        case .cannotGetUserInfo:
            return NSLocalizedString("Unable to load User information from Firebase", comment: "")
        case .cannotUnwrapToMUser:
            return NSLocalizedString("Impossible to convert MUser from User", comment: "")
        }
    }
}
