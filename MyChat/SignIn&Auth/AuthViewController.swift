//
//  AuthViewController.swift
//  MyChat
//
//  Created by Михаил Колотилин on 04.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit
import GoogleSignIn

class AuthViewController: UIViewController {

    let logoLabel = UILabel(text: "MyChat", font: .americanTypewriter40())
    let topGradientView = GradientView(from: .topTrailing,
                                       to: .bottomLeading,
                                       startColor: #colorLiteral(red: 0.7882352941, green: 0.631372549, blue: 0.9411764706, alpha: 1),
                                       endColor: #colorLiteral(red: 0.4784313725, green: 0.6980392157, blue: 0.9215686275, alpha: 1))
    let bottomGradientView = GradientView(from: .topTrailing,
                                          to: .bottomLeading,
                                          startColor: #colorLiteral(red: 0.7882352941, green: 0.631372549, blue: 0.9411764706, alpha: 1),
                                          endColor: #colorLiteral(red: 0.4784313725, green: 0.6980392157, blue: 0.9215686275, alpha: 1))
    
    let googleLabel = UILabel(text: NSLocalizedString("Get started with", comment: ""))
    let emailLabel = UILabel(text: NSLocalizedString("Or sign up with", comment: ""))
    let alreadyOnboardLabel = UILabel(text: NSLocalizedString("Already onboard?", comment: ""))
    
    let googleButton = UIButton(title: "Google", titleColor: .black, backgroundColor: .white, isShadow: true)
    let emailButton = UIButton(title: NSLocalizedString("Email", comment: ""), titleColor: .white, backgroundColor: .buttonDark(), isShadow: false)
    let loginButton = UIButton(title: NSLocalizedString("Login", comment: ""), titleColor: .buttonRed(), backgroundColor: .white, isShadow: true)
    let signUpVC = SignUpViewController()
    let logInVC = LoginViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = .mainWhite()
        googleButton.customizeGoogleButton()
        setupConstraits()
        emailButton.addTarget(self, action: #selector(emailButtonTapped), for: .touchUpInside)
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        googleButton.addTarget(self, action: #selector(googleButtonTapped), for: .touchUpInside)
        
        signUpVC.delegate = self
        logInVC.delegate = self
        
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
    @objc private func emailButtonTapped() {
        present(signUpVC, animated: true, completion: nil)
    }
    
    @objc private func loginButtonTapped() {
        present(logInVC, animated: true, completion: nil)
    }
    
    @objc private func googleButtonTapped() {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
}

// MARK: - Setup Constraits
extension AuthViewController {
    private func setupConstraits() {
        setupLogo()
        
        let googleView = ButtonFormView(label: googleLabel, button: googleButton)
        let emailView = ButtonFormView(label: emailLabel, button: emailButton)
        let loginView = ButtonFormView(label: alreadyOnboardLabel, button: loginButton)
        
        let stackView = UIStackView(arrangedSubviews: [googleView, emailView, loginView], axis: .vertical, spacing: 35)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 40),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40)
        ])
    }

    private func setupLogo() {
        let logoView = UIView(frame: CGRect(x: 0, y: 0, width: 230, height: 46))
        logoView.translatesAutoresizingMaskIntoConstraints = false
        logoLabel.translatesAutoresizingMaskIntoConstraints = false
        topGradientView.translatesAutoresizingMaskIntoConstraints = false
        bottomGradientView.translatesAutoresizingMaskIntoConstraints = false
        
        logoView.backgroundColor = .clear
        
        logoView.addSubview(logoLabel)
        logoView.addSubview(topGradientView)
        logoView.addSubview(bottomGradientView)
        view.addSubview(logoView)
        
        NSLayoutConstraint.activate([
            logoView.bottomAnchor.constraint(equalTo: view.centerYAnchor, constant: -220),
            logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoView.heightAnchor.constraint(equalToConstant: 46),
            logoView.widthAnchor.constraint(equalToConstant: 230)
        ])
        NSLayoutConstraint.activate([
            logoLabel.leadingAnchor.constraint(equalTo: logoView.leadingAnchor),
            logoLabel.centerYAnchor.constraint(equalTo: logoView.centerYAnchor)
        ])
        NSLayoutConstraint.activate([
            topGradientView.trailingAnchor.constraint(equalTo: logoView.trailingAnchor),
            topGradientView.topAnchor.constraint(equalTo: logoView.topAnchor, constant: 10),
            topGradientView.heightAnchor.constraint(equalToConstant: 9),
            topGradientView.widthAnchor.constraint(equalToConstant: 60)
        ])
        NSLayoutConstraint.activate([
            bottomGradientView.trailingAnchor.constraint(equalTo: logoView.trailingAnchor),
            bottomGradientView.bottomAnchor.constraint(equalTo: logoView.bottomAnchor, constant: -10),
            bottomGradientView.heightAnchor.constraint(equalToConstant: 9),
            bottomGradientView.widthAnchor.constraint(equalToConstant: 60)
        ])
    }
}

// MARK: - AuthNavigatingDelegate
extension AuthViewController: AuthNavigatingDelegate {
    func toLoginVC() {
        present(logInVC, animated: true, completion: nil)
    }
    
    func toSignUpVc() {
        present(signUpVC, animated: true, completion: nil)
    }
}

// MARK: - GIDSignInDelegate
extension AuthViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        AuthService.shared.googleLogin(user: user, error: error) { (result) in
            switch result {
            case .success(let user):
                FirestoreService.shared.getUserData(user: user) { (result) in
                    switch result {
                    case .success(let muser):
                        
                        UIApplication.getTopViewController()?.showAlert(with: NSLocalizedString("Success", comment: ""), and: NSLocalizedString("You are authorized", comment: "")) {
                            let mainTabBar = MainTabBarController(currentUser: muser)
                            mainTabBar.modalPresentationStyle = .fullScreen
                            UIApplication.getTopViewController()?.present(mainTabBar, animated: true, completion: nil)
                        }
                    case .failure(let error):
                        UIApplication.getTopViewController()?.showAlert(with: NSLocalizedString("Success", comment: ""), and: NSLocalizedString("You are authorized", comment: "")) {
                            print(error.localizedDescription)
                            let setupProfileVC = SetupProfileViewController(currentUser: user)
                            setupProfileVC.modalPresentationStyle = .fullScreen
                            UIApplication.getTopViewController()?.present(setupProfileVC, animated: true, completion: nil)
                        }
                    }
                }
            case .failure(let error):
                self.showAlert(with: NSLocalizedString("Error", comment: ""), and: error.localizedDescription)
            }
        }
    }
    
    
}

// MARK: - SwiftUI
import SwiftUI

struct AuthVCProvider: PreviewProvider {
    static var previews: some View {
        ContainerView().edgesIgnoringSafeArea(.all)
    }
    
    struct ContainerView: UIViewControllerRepresentable {
        
        let viewController = AuthViewController()
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<AuthVCProvider.ContainerView>) -> AuthViewController {
            return viewController
        }
        func updateUIViewController(_ uiViewController: AuthVCProvider.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<AuthVCProvider.ContainerView>) {
            
        }
    }
}
