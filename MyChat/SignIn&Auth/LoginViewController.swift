//
//  LoginViewController.swift
//  MyChat
//
//  Created by Михаил Колотилин on 08.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit
import GoogleSignIn


class LoginViewController: UIViewController {
    
    let welcomeLabel = UILabel(text: NSLocalizedString("Welcome back!", comment: ""), font: .avenir26())
    
    let loginWithLabel = UILabel(text: NSLocalizedString("Login With", comment: ""))
    let orLabel = UILabel(text: NSLocalizedString("or", comment: ""))
    let emailLabel = UILabel(text: NSLocalizedString("Email", comment: ""))
    let passwordLabel = UILabel(text: NSLocalizedString("Password", comment: ""))
    let needAnAccountLabel = UILabel(text: NSLocalizedString("Need an account?", comment: ""))
    
    let googleButton = UIButton(title: "Google", titleColor: .black, backgroundColor: .white, isShadow: true)
    
    let emailTextField = OneLineTextField(font: .avenir20())
    let passwordTextField = OneLineTextField(font: .avenir20())
    
    let loginButton = UIButton(title: NSLocalizedString("Login", comment: ""), titleColor: .white, backgroundColor: .buttonDark(), isShadow: false)
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(NSLocalizedString("Sign Up", comment: ""), for: .normal)
        button.setTitleColor(.buttonRed(), for: .normal)
        button.titleLabel?.font = .avenir20()
        button.addTarget(self, action: #selector(signUpButtonTapped), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: AuthNavigatingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .mainWhite()
        self.hideKeyboardWhenTappedAround()
        googleButton.customizeGoogleButton()
        setupConstraits()
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        googleButton.addTarget(self, action: #selector(googleButtonTapped), for: .touchUpInside)
    }
    
    @objc private func googleButtonTapped() {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @objc private func loginButtonTapped() {
        AuthService.shared.login(email: emailTextField.text!,
                                 password: passwordTextField.text!) { (result) in
                                    switch result {
                                    case .success(let user):
                                        self.showAlert(with: "Success", and: "You are authorized") {
                                            FirestoreService.shared.getUserData(user: user) { (result) in
                                                switch result {
                                                case .success(let muser):
                                                    let mainTabBar = MainTabBarController(currentUser: muser)
                                                    mainTabBar.modalPresentationStyle = .fullScreen
                                                    self.present(mainTabBar, animated: true, completion: nil)
                                                case .failure(let error):
                                                    print(error.localizedDescription)
                                                    let setupProfileVC = SetupProfileViewController(currentUser: user)
                                                    setupProfileVC.modalPresentationStyle = .fullScreen
                                                    self.present(setupProfileVC, animated: true, completion: nil)                                                }
                                            }
                                        }
                                        print(user.email!)
                                    case .failure(let error):
                                        self.showAlert(with: NSLocalizedString("Error", comment: ""), and: error.localizedDescription)
                                    }
        }
    }
    
    @objc private func signUpButtonTapped() {
        dismiss(animated: true) {
            self.delegate?.toSignUpVc()
        }
        
    }
}

// MARK: - Setup Constraits
extension LoginViewController {
    private func setupConstraits() {
        let loginWithView = ButtonFormView(label: loginWithLabel, button: googleButton)
        let emailStackView = UIStackView(arrangedSubviews: [emailLabel,emailTextField],
                                         axis: .vertical,
                                         spacing: 0)
        let passwordStackView = UIStackView(arrangedSubviews: [passwordLabel,passwordTextField],
                                            axis: .vertical,
                                            spacing: 0)
        loginButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        let stackView = UIStackView(arrangedSubviews: [
            loginWithView,
            orLabel,
            emailStackView,
            passwordStackView,
            loginButton
        ], axis: .vertical, spacing: 30)
        
        signUpButton.contentHorizontalAlignment = .leading
        let bottomStackView = UIStackView(arrangedSubviews: [needAnAccountLabel, signUpButton], axis: .horizontal, spacing: 10)
        bottomStackView.alignment = .firstBaseline
        
        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        bottomStackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(welcomeLabel)
        view.addSubview(stackView)
        view.addSubview(bottomStackView)
        
        NSLayoutConstraint.activate([
            welcomeLabel.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -40),
            welcomeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40)
        ])
        NSLayoutConstraint.activate([
            bottomStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            bottomStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            bottomStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40)
        ])
    }
}

// MARK: - SwiftUI
import SwiftUI

struct LoginVCProvider: PreviewProvider {
    static var previews: some View {
        ContainerView()
            .edgesIgnoringSafeArea(.all)
            .environment(\.colorScheme, .light)
    }
    
    struct ContainerView: UIViewControllerRepresentable {
        
        let loginVC = LoginViewController()
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<LoginVCProvider.ContainerView>) -> LoginViewController {
            return loginVC
        }
        func updateUIViewController(_ uiViewController: LoginVCProvider.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<LoginVCProvider.ContainerView>) {
            
        }
    }
}
