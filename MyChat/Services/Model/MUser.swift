//
//  MUser.swift
//  MyChat
//
//  Created by Михаил Колотилин on 10.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit
import FirebaseFirestore

struct MUser: Hashable, Decodable {
    var firstname: String
    var lastname: String
    var fullname: String
    var email: String
    var avatarStringURL: String
    var description: String
    var sex: String
    var id: String
    
    //MARK: - Initializers
    init(firstname: String, lastname: String, email: String, avatarStringURL: String, description: String, sex: String, id: String) {
        self.firstname = firstname
        self.lastname = lastname
        self.fullname = firstname + " " + lastname
        self.email = email
        self.avatarStringURL = avatarStringURL
        self.description = description
        self.sex = sex
        self.id = id
    }
    
    init?(document: DocumentSnapshot) {
        guard let data = document.data() else { return nil }
        guard let firstname = data["firstname"] as? String,
            let lastname = data["lastname"] as? String,
            let fullname = data["fullname"] as? String,
            let email = data["email"] as? String,
            let avatarStringURL = data["avatarStringURL"] as? String,
            let description = data["description"] as? String,
            let sex = data["sex"] as? String,
            let id = data["uid"] as? String else { return nil }
        
        self.firstname = firstname
        self.lastname = lastname
        self.fullname = fullname
        self.email = email
        self.avatarStringURL = avatarStringURL
        self.description = description
        self.sex = sex
        self.id = id
    }
    
    init?(document: QueryDocumentSnapshot) {
        let data = document.data()
        guard let firstname = data["firstname"] as? String,
            let lastname = data["lastname"] as? String,
            let fullname = data["fullname"] as? String,
            let email = data["email"] as? String,
            let avatarStringURL = data["avatarStringURL"] as? String,
            let description = data["description"] as? String,
            let sex = data["sex"] as? String,
            let id = data["uid"] as? String else { return nil }
        
        self.firstname = firstname
        self.lastname = lastname
        self.fullname = fullname
        self.email = email
        self.avatarStringURL = avatarStringURL
        self.description = description
        self.sex = sex
        self.id = id
    }
    
    var representation: [String: Any] {
        var rep = ["firstname": firstname]
        rep["lastname"] = lastname
        rep["fullname"] = fullname
        rep["email"] = email
        rep["avatarStringURL"] = avatarStringURL
        rep["description"] = description
        rep["sex"] = sex
        rep["uid"] = id
        return rep
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: MUser, rhs: MUser) -> Bool {
        return lhs.id == rhs.id
    }
    
    func contains(filter: String?) -> Bool {
        guard let filter = filter else { return true }
        if filter.isEmpty { return true }
        let lowercasedFilter = filter.lowercased()
        return fullname.lowercased().contains(lowercasedFilter)
    }
}
